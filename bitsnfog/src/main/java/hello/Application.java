package hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan
public class Application {

    @Bean
 
    MessageService ramanaMessage() {
        return new MessageService() {
            public String getMessage() {
              return "Hello World from Ramana Message !";
            }
        };
    }
    
    @Bean
    @Primary
    MessageService madhaviMessage() {
        return new MessageService() {
            public String getMessage() {
              return "Hello World from Madhavi Message !";
            }
        };
    }
    
 
  public static void main(String[] args) {
	  System.out.println("this is 10:46");
      ApplicationContext context = 
          new AnnotationConfigApplicationContext(Application.class);
      MessagePrinter printer = context.getBean(MessagePrinter.class);
      
  
     
      printer.printMessage();
 
  }
}
