package hibernate.dto;



import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

public class GetApp {

	public static void main(String[] args) {
		UserDetails ud = new UserDetails();
	//	Criteria API			
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(UserDetails.class);
		criteria.add(Restrictions.eq("userId", 1));
		List<UserDetails> users = (List<UserDetails>) criteria.list();
	   	    
	    users.forEach((x) ->
	     { System.out.println("record fetched - "+x.getUserId());});
			
	    
	    	
	    
	    

	}

}
