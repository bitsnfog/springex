package hibernate.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

//@Entity
@Embeddable
@Table (name="CONTACT_INFO")
public class ContactInfo {
	@Column (name="PHONE_NUMBER")
	private String phoneNumber;
	
	@Column (name="CONTACT_TYPE")
	private String contactType;

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	

}
