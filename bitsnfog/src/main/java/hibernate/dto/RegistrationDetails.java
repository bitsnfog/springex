package hibernate.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="REGISTRATION_DETAILS")
public class RegistrationDetails {
	@Id
	@Column (name="REGISTRATION_ID")
	@GeneratedValue (strategy=GenerationType.AUTO) 
	private Integer registartionId;
	private String registrationDetails;
	
	public Integer getRegistartionId() {
		return registartionId;
	}
	public void setRegistartionId(Integer registartionId) {
		this.registartionId = registartionId;
	}
	public String getRegistrationDetails() {
		return registrationDetails;
	}
	public void setRegistrationDetails(String registrationDetails) {
		this.registrationDetails = registrationDetails;
	}
	
	

}
