package hibernate.dto;



import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SaveApp {

	public static void main(String[] args) {
		UserDetails ud = new UserDetails();
		RegistrationDetails rd = new RegistrationDetails();
		rd.setRegistrationDetails("Fist Registration at 9:56");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		//session.save(rd);
		ud.setRegistrationDetails(rd);
	//	ud.setUserId(1);
		ud.setUserName("First User - 2:34");
		Address addr = new Address();
		addr.setAddressLine_1("Addresss Line1");
		addr.setCity("Moline");
		addr.setZip("52722");
		ud.setAddress(addr);
		ud.setJoinDate(new Date());
		ud.setDescription("User Description");
		ContactInfo ci = new ContactInfo();
		ContactInfo ci1 = new ContactInfo();
		ci.setPhoneNumber("309-716-5965");
		ci.setContactType("Work");
		ud.getContactInfo().add(ci);
		ci1.setPhoneNumber("563-209-9449");
		ci1.setContactType("Home");
		ud.getContactInfo().add(ci1);
						
		
		session.persist(ud);
	    session.getTransaction().commit();
	    session.close();
	   
	   

	}

}
