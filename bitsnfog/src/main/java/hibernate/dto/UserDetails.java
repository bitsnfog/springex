package hibernate.dto;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;



import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.engine.spi.CascadingAction;

@Entity
@Cacheable
@Cache (usage = CacheConcurrencyStrategy.READ_ONLY)
@Table (name="USER_DETAILS")
public class UserDetails {
	@Id
	@Column (name="USER_ID")
	@GeneratedValue (strategy=GenerationType.AUTO) 
	private int userId;
	@Column (name="USER_NAME")
	private String userName;
	@Embedded
	private Address address;
	
	@Embedded
	@AttributeOverrides({
	@AttributeOverride (name="addressLine_1",column=@Column(name="WORK_ADDRESS_1")),
	@AttributeOverride (name="city",column=@Column(name="WORK_CITY")),
	@AttributeOverride (name="zip",column=@Column(name="WORK_ZIP"))
	})
	private Address workaddress;
	
	public Address getWorkaddress() {
		return workaddress;
	}
	public void setWorkaddress(Address workaddress) {
		this.workaddress = workaddress;
	}
	@OneToOne (cascade=CascadeType.PERSIST)
	@JoinColumn(name="REGISTRATION_ID")
	private RegistrationDetails registrationDetails;
	
	public RegistrationDetails getRegistrationDetails() {
		return registrationDetails;
	}
	public void setRegistrationDetails(RegistrationDetails registrationDetails) {
		this.registrationDetails = registrationDetails;
	}
	@Column (name="DESCRIPTION")
	private String description;
	@Column (name="JOINDATE")
//	@Temporal (TemporalType.DATE);
	private Date joinDate;
	@Transient 
	private String donotinsert;
	
//  Collection
	@ElementCollection
	@JoinTable (name="CONTACT_INFO",
	            joinColumns = @JoinColumn(name="USER_ID"))
	@GenericGenerator(name="seq", strategy="increment")
	@CollectionId(columns = { @Column (name="ADDRESS_ID")}, generator = "seq", type=@Type(type="long"))
	private Collection<ContactInfo> contactInfo = new ArrayList();
	
	
	public void setContactInfo(Collection<ContactInfo> contactInfo) {
		this.contactInfo = contactInfo;
	}
	public Collection<ContactInfo> getContactInfo() {
		return contactInfo;
	}
	
	public String getDonotinsert() {
		return donotinsert;
	}
	public void setDonotinsert(String donotinsert) {
		this.donotinsert = donotinsert;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
