package example;

import com.amazonaws.services.lambda.runtime.Context; 
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Hello {
    public String handleRequest(Customer  key1, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("received : "+key1.getName());
        return "Hello from lambda"+key1.getName();
    }
}