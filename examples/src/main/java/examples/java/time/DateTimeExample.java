package examples.java.time;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.DayOfWeek;

import java.time.temporal.*;
import java.time.temporal.ChronoField.*;

public class DateTimeExample
{
    public static void main( String[] args ) throws InterruptedException
    {
    	DateTimeExample dt = new DateTimeExample();
    	//To calculate elapsed time to measure performance
    	dt.displayDates();
    	dt.timeElapsed();
    	dt.ClockExample();
    }


public void timeElapsed() throws InterruptedException {
	System.out.println("------------ Calculate Duration -------------------------------------------------------"); 
	Instant beginTime = Instant.now();
	System.out.println("Start Time: "+beginTime);
	
	Thread.sleep(50);
	
	Instant endTime = Instant.now();
	System.out.println("End Time: "+endTime);
	
//	Duration elapsedTime = Duration.between(beginTime,endTime);
 	System.out.println( "Total elapsed (ms) - Duration : "+Duration.between(beginTime,endTime).toMillis());
 	//System.out.println( "Total elapsed (ms) Period : "+Period.);
	
}

public void ClockExample()  {
	
	Clock ck;
	System.out.println(Instant.now());
	
	
}

public void displayDates()  {
	
	System.out.println("------------Display Date & Time -------------------------------------------------------"); 
	
	System.out.println("Instant           : "+Instant.now());
	System.out.println("Local Date        : "+LocalDate.now());
	System.out.println("Local Date of     : "+LocalDate.of(2016,12,01));
	
	System.out.println("Local Time        : "+LocalTime.now());
	System.out.println("Local Date & Time : "+LocalDateTime.now());
	System.out.println("Zoned Date & Time : "+ZonedDateTime.now());
		
}

public static boolean isFriday13(TemporalAccessor ta) {
    if (ta.isSupported(ChronoField.DAY_OF_MONTH) && ta.isSupported(ChronoField.DAY_OF_WEEK)) {
        int dayOfMonth = ta.get(ChronoField.DAY_OF_MONTH);
        int weekDay = ta.get(ChronoField.DAY_OF_WEEK);
        DayOfWeek dayOfWeek = DayOfWeek.of(weekDay);
        if (dayOfMonth == 13 && dayOfWeek == DayOfWeek.FRIDAY) {
            return true;
        }
    }
    return false;
}

}
