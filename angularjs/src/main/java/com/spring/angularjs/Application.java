package com.spring.angularjs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableCaching
public class Application {

	//private static final Logger log = LoggerFactory.getLogger(Application.class);

	

	public static void main(String[] args) {
		
		SpringApplication app = new SpringApplication(Application.class);
	    app.setBannerMode(Banner.Mode.OFF);
	    app.run(args);
	//	SpringApplication.run(Application.class);
		
		
	}

	
	}

