package com.spring.angularjs;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.time.Duration;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@Configurable
@RequestMapping(value = {"/","/hero"})

@CrossOrigin
public class HeroController  {

	@Autowired
	HeroRepository hr;
	@Autowired 
	UtilityBean ub;

	@RequestMapping(method=	RequestMethod.GET)
	
    public ArrayList getAllHeros() {
		ub.startTime();
        ArrayList<Hero> Hero1 = new ArrayList();
    	hr.findAll().forEach(a -> Hero1.add(a));
    	ub.displayDuration("GET /Hero ");
  
     	return Hero1;
     
    }
	
	@RequestMapping(value="/error",  method=RequestMethod.GET)
    public String displayError() {
    	    	
    	return "Method not implemented";
     
    }
   
	@RequestMapping(value="/{heroId}",  method=	RequestMethod.GET)
    public Hero getOneHero(@PathVariable("heroId") Integer heroId) {
		ub.startTime();
    	Hero newHero;
    	newHero = hr.findOne(heroId);
    	ub.displayDuration("GET /Hero/"+heroId);
       	return newHero;
	     
    }
	
	@RequestMapping(value="Name/{heroName}",  method =	RequestMethod.GET)
    public List getByHeroName(@PathVariable("heroName") String heroName) {
		ub.startTime();
        List<Hero> Hero1 = new ArrayList();
     	
    	Hero1 = hr.findByLastname(heroName);
    	ub.displayDuration("GET /Hero/"+heroName);
    	
    	return Hero1;
	     
    }

	
	@RequestMapping(value="/{heroId}",  method=	RequestMethod.DELETE)
    public Hero deleteHero(@PathVariable("heroId") Integer heroId) {
		ub.startTime();
   
     	Hero newHero;
    	hr.delete(heroId);
    	ub.displayDuration("DELETE /Hero/"+heroId);
    	return null;

    }
		
	@RequestMapping(method=	RequestMethod.POST,  consumes = "application/json")
	@CrossOrigin
    public Hero updateHero(@RequestBody Hero hero) {
		ub.startTime();
	  	Hero newHero;
    	hero = hr.save(hero);
      	ub.displayDuration("POST /Hero/");
	return hero;
	     
    }
		
	@RequestMapping(method=	RequestMethod.PUT,  consumes = "application/json")

    public Hero addHero(@RequestBody Hero hero) {
		ub.startTime();
    
     	Hero newHero;
     	hero._id = null;
    	hero = hr.save(hero);
    	ub.displayDuration("PUT /Hero/");
    	
    	return hero;
	     
    }

		
}
