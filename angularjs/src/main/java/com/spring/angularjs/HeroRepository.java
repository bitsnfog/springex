package com.spring.angularjs;


import java.util.List;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;



public interface HeroRepository extends CrudRepository<Hero, Integer> {
	
	//@Cacheable
	List<Hero> findByLastname(String lastName);
	
	//@Cacheable
	List<Hero> findAll();
	
	//@Cacheable
	Hero findOne(Integer _id);
	

    
}
