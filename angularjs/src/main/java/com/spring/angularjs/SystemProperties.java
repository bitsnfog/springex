package com.spring.angularjs;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="CUSTOM")
public class SystemProperties {
	String computer = "SONY";

	public String getComputer() {
		return computer;
	}

	public void setComputer(String computer) {
		this.computer = computer;
	}
	

}
